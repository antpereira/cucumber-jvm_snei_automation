Feature: SNEI American Express No CC

  V3.1 charge input should be able to create new customer if a customer has not already exist

    ########################################################################################################################
    #                                     CHARGE Transaction                                                               #
    ########################################################################################################################


  Scenario Outline: A charge for American Express is being made wtihout passing CVV

    When I do a call to set the signature and use the signature to post a charge wtihout passing a CVV with <paymentMethod>, <CreditCardNo>, <paymentConfigId>, <instrumentConfigId> and <acceptedCommand>
    Then I get a CHARGED transaction for either EBANX/ASTROPAY Validations for CHARGE

    Examples:
    | paymentMethod    | CreditCardNo     | paymentConfigId | instrumentConfigId | acceptedCommand |
    | AMERICAN_EXPRESS | 378282246310005  | 1002            | 1008               | CHARGE          |
    | AMERICAN_EXPRESS | 378282246310005  | 1004            | 1042               | CHARGE          |


    ########################################################################################################################
    #                              REFUND for a CHARGED Transaction                                                        #
    ########################################################################################################################


  Scenario Outline: A charge for American Express is being made and REFUND command is being made to the charge wtihout passing CVV

    When I do a call to set the signature and use the signature to post a charge wtihout passing a CVV with <paymentMethod>, <CreditCardNo>, <paymentConfigId>, <instrumentConfigId> and <acceptedCommand>
    And I do a call to update the CHARGED transaction above with the <updateCommand>
    Then I get a REFUNDED transaction for either EBANX/ASTROPAY Validations for REFUND

    Examples:
      | paymentMethod    | CreditCardNo     | paymentConfigId | instrumentConfigId | acceptedCommand | updateCommand |
      | AMERICAN_EXPRESS | 378282246310005  | 1002            | 1008               | CHARGE          | REFUND        |
      | AMERICAN_EXPRESS | 378282246310005  | 1004            | 1042               | CHARGE          | REFUND        |


    ########################################################################################################################
    #                              CHARGEBACK for a CHARGED Transaction                                                    #
    ########################################################################################################################


  Scenario Outline: A charge for American Express is being made and CHARGEBACK command is being made to the charge wtihout passing CVV

    When I do a call to set the signature and use the signature to post a charge wtihout passing a CVV with <paymentMethod>, <CreditCardNo>, <paymentConfigId>, <instrumentConfigId> and <acceptedCommand>
    And I do a call to update the CHARGED transaction above with the <updateCommand>
    Then I get a CHARGED_BACK transaction for either EBANX/ASTROPAY Validations for CHARGEBACK

    Examples:
      | paymentMethod    | CreditCardNo     | paymentConfigId | instrumentConfigId | acceptedCommand | updateCommand |
      | AMERICAN_EXPRESS | 378282246310005  | 1002            | 1008               | CHARGE          | CHARGEBACK    |
      | AMERICAN_EXPRESS | 378282246310005  | 1004            | 1042               | CHARGE          | CHARGEBACK    |


    ########################################################################################################################
    #                              REVERSE_CHARGEBACK for a CHARGED_BACK Transaction                                       #
    ########################################################################################################################


  Scenario Outline: A charge for American Express is being made and REVERSE_CHARGEBACK command is being made to the charge wtihout passing CVV

    When I do a call to set the signature and use the signature to post a charge wtihout passing a CVV with <paymentMethod>, <CreditCardNo>, <paymentConfigId>, <instrumentConfigId> and <acceptedCommand>
    And I do a call to update the CHARGED transaction above with the <updateCommand>
    And I do a call to update the CHARGED_BACK transaction above with the <revchargeback_command>
    Then I get a CHARGEBACK_REVERSED transaction for either EBANX/ASTROPAY Validations for REVERSE_CHARGEBACK

    Examples:
      | paymentMethod    | CreditCardNo     | paymentConfigId | instrumentConfigId | acceptedCommand | updateCommand | revchargeback_command |
      | AMERICAN_EXPRESS | 378282246310005  | 1002            | 1008               | CHARGE          | CHARGEBACK    | REVERSE_CHARGEBACK    |
      | AMERICAN_EXPRESS | 378282246310005  | 1004            | 1042               | CHARGE          | CHARGEBACK    | REVERSE_CHARGEBACK    |


    ########################################################################################################################
    #                                     Funds_Auth Transaction                                                           #
    ########################################################################################################################


  Scenario Outline: A FUNDS_AUTH for American Express is being processed wtihout passing CVV

    When I do a call to set the signature and use the signature to post a charge wtihout passing a CVV with <paymentMethod>, <CreditCardNo>, <paymentConfigId>, <instrumentConfigId> and <acceptedCommand>
    Then I get a FUNDS_AUTHORIZED transaction for either EBANX/ASTROPAY Validations for FUNDS_AUTH

    Examples:
      | paymentMethod    | CreditCardNo     | paymentConfigId | instrumentConfigId | acceptedCommand     |
      | AMERICAN_EXPRESS | 378282246310005  | 1002            | 1008               | FUNDS_AUTHORIZATION |
      | AMERICAN_EXPRESS | 378282246310005  | 1004            | 1042               | FUNDS_AUTHORIZATION |


    ########################################################################################################################
    #                              CANCEL_FUNDS_AUTH for a FUNDS_AUTH Transaction                                          #
    ########################################################################################################################


  Scenario Outline: A FUNDS_AUTH for American Express is being made and CANCEL_FUNDS_AUTH command is being processed wtihout passing CVV

    When I do a call to set the signature and use the signature to post a charge wtihout passing a CVV with <paymentMethod>, <CreditCardNo>, <paymentConfigId>, <instrumentConfigId> and <acceptedCommand>
    And I do a call to update the FUNDS_AUTH transaction above with the <updateCommand>
    Then I get a SUBMITTED transaction for either EBANX/ASTROPAY Validations for CANCEL_FUNDS_AUTHORIZATION

    Examples:
      | paymentMethod    | CreditCardNo     | paymentConfigId | instrumentConfigId | acceptedCommand     | updateCommand              |
      | AMERICAN_EXPRESS | 378282246310005  | 1002            | 1008               | FUNDS_AUTHORIZATION | CANCEL_FUNDS_AUTHORIZATION |
      | AMERICAN_EXPRESS | 378282246310005  | 1004            | 1042               | FUNDS_AUTHORIZATION | CANCEL_FUNDS_AUTHORIZATION |


    ########################################################################################################################
    #                              CAPTURE_FUNDS for a FUNDS_AUTH Transaction                                              #
    ########################################################################################################################


  Scenario Outline: A FUNDS_AUTH for American Express is being made and CAPTURE_FUNDS command is being processed wtihout passing CVV

    When I do a call to set the signature and use the signature to post a charge wtihout passing a CVV with <paymentMethod>, <CreditCardNo>, <paymentConfigId>, <instrumentConfigId> and <acceptedCommand>
    And I do a call to update the FUNDS_AUTH transaction above with the <updateCommand>
    Then I get a CHARGED transaction for either EBANX/ASTROPAY Validations for CAPTURE_FUNDS

    Examples:
      | paymentMethod    | CreditCardNo     | paymentConfigId | instrumentConfigId | acceptedCommand     | updateCommand |
      | AMERICAN_EXPRESS | 378282246310005  | 1002            | 1008               | FUNDS_AUTHORIZATION | CAPTURE_FUNDS |
      | AMERICAN_EXPRESS | 378282246310005  | 1004            | 1042               | FUNDS_AUTHORIZATION | CAPTURE_FUNDS |


    ########################################################################################################################
    #                              REFUND for a FUNDS_AUTH -> CAPTURE_FUNDS Transaction                                    #
    ########################################################################################################################


  Scenario Outline: A FUNDS_AUTH for American Express is being made and CAPTURE_FUNDS command is being processed and i try to REFUND the transaction wtihout passing CVV

    When I do a call to set the signature and use the signature to post a charge wtihout passing a CVV with <paymentMethod>, <CreditCardNo>, <paymentConfigId>, <instrumentConfigId> and <acceptedCommand>
    And I do a call to update the FUNDS_AUTH transaction above with the <updateCommand>
    And I do a call to update the FUNDS_AUTH transaction above with the <RefundCommand>
    Then I get a REFUNDED transaction for either EBANX/ASTROPAY Validations for REFUND

    Examples:
      | paymentMethod    | CreditCardNo     | paymentConfigId | instrumentConfigId | acceptedCommand     | updateCommand | RefundCommand |
      | AMERICAN_EXPRESS | 378282246310005  | 1002            | 1008               | FUNDS_AUTHORIZATION | CAPTURE_FUNDS | REFUND        |
      | AMERICAN_EXPRESS | 378282246310005  | 1004            | 1042               | FUNDS_AUTHORIZATION | CAPTURE_FUNDS | REFUND        |


    ########################################################################################################################
    #                              CHARGEBACK for a FUNDS_AUTH -> CAPTURE_FUNDS Transaction                                #
    ########################################################################################################################


  Scenario Outline: A FUNDS_AUTH for American Express is being made and CAPTURE_FUNDS command is being processed and i try to CHARGEBACK the transaction wtihout passing CVV

    When I do a call to set the signature and use the signature to post a charge wtihout passing a CVV with <paymentMethod>, <CreditCardNo>, <paymentConfigId>, <instrumentConfigId> and <acceptedCommand>
    And I do a call to update the FUNDS_AUTH transaction above with the <updateCommand>
    And I do a call to update the FUNDS_AUTH transaction above with the <ChargebackCommand>
    Then I get a CHARGED_BACK transaction for either EBANX/ASTROPAY Validations for CHARGEBACK

    Examples:
      | paymentMethod    | CreditCardNo     | paymentConfigId | instrumentConfigId | acceptedCommand     | updateCommand | ChargebackCommand |
      | AMERICAN_EXPRESS | 378282246310005  | 1002            | 1008               | FUNDS_AUTHORIZATION | CAPTURE_FUNDS | CHARGEBACK        |
      | AMERICAN_EXPRESS | 378282246310005  | 1004            | 1042               | FUNDS_AUTHORIZATION | CAPTURE_FUNDS | CHARGEBACK        |


    ########################################################################################################################
    #                              REVERSE_CHARGEBACK for a FUNDS_AUTH -> CAPTURE_FUNDS Transaction                        #
    ########################################################################################################################


  Scenario Outline: A FUNDS_AUTH for American Express is being made and CAPTURE_FUNDS command is being processed and i try to REVERSE_CHARGEBACK the transaction wtihout passing CVV

    When I do a call to set the signature and use the signature to post a charge wtihout passing a CVV with <paymentMethod>, <CreditCardNo>, <paymentConfigId>, <instrumentConfigId> and <acceptedCommand>
    And I do a call to update the FUNDS_AUTH transaction above with the <updateCommand>
    And I do a call to update the FUNDS_AUTH transaction above with the <ChargebackCommand>
    And I do a call to update the FUNDS_AUTH transaction above with the <revchargeback_command>
    Then I get a CHARGEBACK_REVERSED transaction for either EBANX/ASTROPAY Validations for REVERSE_CHARGEBACK


    Examples:
      | paymentMethod    | CreditCardNo     | paymentConfigId | instrumentConfigId | acceptedCommand     | updateCommand | ChargebackCommand | revchargeback_command |
      | AMERICAN_EXPRESS | 378282246310005  | 1002            | 1008               | FUNDS_AUTHORIZATION | CAPTURE_FUNDS | CHARGEBACK        | REVERSE_CHARGEBACK    |
      | AMERICAN_EXPRESS | 378282246310005  | 1004            | 1042               | FUNDS_AUTHORIZATION | CAPTURE_FUNDS | CHARGEBACK        | REVERSE_CHARGEBACK    |










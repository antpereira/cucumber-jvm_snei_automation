Feature: SNEI Invalid Field Validations for Mastercard

  V3.1 charge input should be able to create new customer if a customer has not already exist

##################################################################################################
#    There is no amount validation for EBANX. Transaction gets CHARGED with invalid data         #
#    There is no StateProvince validation for EBANX. Transaction gets CHARGED with invalid data  #
##################################################################################################


###################################################
#    1002/1007 - Ebanx Mastercard                 #
#    1004/1041 - Astropay Mastercard              #
###################################################


    ########################################################################################################################
    #                           Invalid Fields Transaction and Validations                                                 #
    ########################################################################################################################


  Scenario Outline: A charge for Credit Card is being made and the response for error is validated for invalid fields

    When I do a call to post a charge with missing/invalid fields for <paymentMethod>, <CreditCardNo>, <paymentConfigId>, <instrumentConfigId>, <acceptedCommand>, <orignalFieldValue> and <replacedValue>
    And I will validate the <responseValidation> for the above charge

    Examples:
      | paymentMethod | CreditCardNo     | paymentConfigId | instrumentConfigId | acceptedCommand | responseValidation | orignalFieldValue | replacedValue      |
      | MASTERCARD    | 5105105105105100 | 1002            | 1007               | CHARGE          | invalidCardNumber  | 5105105105105100  | 12345              |
      | MASTERCARD    | 5105105105105100 | 1002            | 1007               | CHARGE          | invalidExpMonth    | 112018            | 132018             |
      | MASTERCARD    | 5105105105105100 | 1002            | 1007               | CHARGE          | invalidCVV         | 321               | 12345              |
      | MASTERCARD    | 5105105105105100 | 1002            | 1007               | CHARGE          | invalidAmount      | 300               | 11111111111112222  |
      | MASTERCARD    | 5105105105105100 | 1002            | 1007               | CHARGE          | invalidCountry     | BR<               | IN<                |
      | MASTERCARD    | 5105105105105100 | 1002            | 1007               | CHARGE          | invalidCurrency    | BRL               | INL                |
      | MASTERCARD    | 5105105105105100 | 1002            | 1007               | CHARGE          | invalidCPF         | 031.138.219-32    | 123                |
      | MASTERCARD    | 5105105105105100 | 1002            | 1007               | CHARGE          | invalidEmail       | tony@gmail.com    | 123@111            |
      | MASTERCARD    | 5105105105105100 | 1002            | 1007               | CHARGE          | invalidPhoneNo     | 1234567890        | !@123              |
      | MASTERCARD    | 5105105105105100 | 1002            | 1007               | CHARGE          | invalidState       | PR                | PRESS              |

    Examples:
      | paymentMethod | CreditCardNo     | paymentConfigId | instrumentConfigId | acceptedCommand | responseValidation | orignalFieldValue | replacedValue      |
      | MASTERCARD    | 5105105105105100 | 1004            | 1041               | CHARGE          | invalidCardNumber  | 5105105105105100  | 12345              |
      | MASTERCARD    | 5105105105105100 | 1004            | 1041               | CHARGE          | invalidExpMonth    | 112018            | 132018             |
      | MASTERCARD    | 5105105105105100 | 1004            | 1041               | CHARGE          | invalidCVV         | 321               | 12345              |
      | MASTERCARD    | 5105105105105100 | 1004            | 1041               | CHARGE          | invalidAmount      | 300               | 11111111111112222  |
      | MASTERCARD    | 5105105105105100 | 1004            | 1041               | CHARGE          | invalidCountry     | BR<               | IN<                |
      | MASTERCARD    | 5105105105105100 | 1004            | 1041               | CHARGE          | invalidCurrency    | BRL               | INL                |
      | MASTERCARD    | 5105105105105100 | 1004            | 1041               | CHARGE          | invalidCPF         | 031.138.219-32    | 123                |
      | MASTERCARD    | 5105105105105100 | 1004            | 1041               | CHARGE          | invalidEmail       | tony@gmail.com    | 123@111            |
      | MASTERCARD    | 5105105105105100 | 1004            | 1041               | CHARGE          | invalidPhoneNo     | 1234567890        | !@123              |
      | MASTERCARD    | 5105105105105100 | 1004            | 1041               | CHARGE          | invalidState       | PR                | PRESS              |


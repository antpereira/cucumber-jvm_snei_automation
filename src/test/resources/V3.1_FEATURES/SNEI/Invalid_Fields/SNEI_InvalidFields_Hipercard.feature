Feature: SNEI Invalid Field Validations for Hipercard

  V3.1 charge input should be able to create new customer if a customer has not already exist

##################################################################################################
#    There is no amount validation for EBANX. Transaction gets CHARGED with invalid data         #
#    There is no StateProvince validation for EBANX. Transaction gets CHARGED with invalid data  #
##################################################################################################


###################################################
#    1002/1022 - Ebanx Hipercard                  #
#    1004/1025 - Astropay Hipercard               #
###################################################


    ########################################################################################################################
    #                           Invalid Fields Transaction and Validations                                                 #
    ########################################################################################################################


  Scenario Outline: A charge for Credit Card is being made and the response for error is validated for invalid fields

    When I do a call to post a charge with missing/invalid fields for <paymentMethod>, <CreditCardNo>, <paymentConfigId>, <instrumentConfigId>, <acceptedCommand>, <orignalFieldValue> and <replacedValue>
    And I will validate the <responseValidation> for the above charge

    Examples:
      | paymentMethod | CreditCardNo     | paymentConfigId | instrumentConfigId | acceptedCommand | responseValidation | orignalFieldValue | replacedValue      |
      | HIPERCARD     | 6062825624254001 | 1002            | 1022               | CHARGE          | invalidCardNumber  | 6062825624254001  | 12345              |
      | HIPERCARD     | 6062825624254001 | 1002            | 1022               | CHARGE          | invalidExpMonth    | 112018            | 132018             |
      | HIPERCARD     | 6062825624254001 | 1002            | 1022               | CHARGE          | invalidCVV         | 321               | 12345              |
      | HIPERCARD     | 6062825624254001 | 1002            | 1022               | CHARGE          | invalidAmount      | 300               | 11111111111112222  |
      | HIPERCARD     | 6062825624254001 | 1002            | 1022               | CHARGE          | invalidCountry     | BR<               | IN<                |
      | HIPERCARD     | 6062825624254001 | 1002            | 1022               | CHARGE          | invalidCurrency    | BRL               | INL                |
      | HIPERCARD     | 6062825624254001 | 1002            | 1022               | CHARGE          | invalidCPF         | 031.138.219-32    | 123                |
      | HIPERCARD     | 6062825624254001 | 1002            | 1022               | CHARGE          | invalidEmail       | tony@gmail.com    | 123@111            |
      | HIPERCARD     | 6062825624254001 | 1002            | 1022               | CHARGE          | invalidPhoneNo     | 1234567890        | !@123              |
      | HIPERCARD     | 6062825624254001 | 1002            | 1022               | CHARGE          | invalidState       | PR                | PRESS              |

    Examples:
      | paymentMethod | CreditCardNo     | paymentConfigId | instrumentConfigId | acceptedCommand | responseValidation | orignalFieldValue | replacedValue      |
      | HIPERCARD     | 6062825624254001 | 1004            | 1025               | CHARGE          | invalidCardNumber  | 6062825624254001  | 12345              |
      | HIPERCARD     | 6062825624254001 | 1004            | 1025               | CHARGE          | invalidExpMonth    | 112018            | 132018             |
      | HIPERCARD     | 6062825624254001 | 1004            | 1025               | CHARGE          | invalidCVV         | 321               | 12345              |
      | HIPERCARD     | 6062825624254001 | 1004            | 1025               | CHARGE          | invalidAmount      | 300               | 11111111111112222  |
      | HIPERCARD     | 6062825624254001 | 1004            | 1025               | CHARGE          | invalidCountry     | BR<               | IN<                |
      | HIPERCARD     | 6062825624254001 | 1004            | 1025               | CHARGE          | invalidCurrency    | BRL               | INL                |
      | HIPERCARD     | 6062825624254001 | 1004            | 1025               | CHARGE          | invalidCPF         | 031.138.219-32    | 123                |
      | HIPERCARD     | 6062825624254001 | 1004            | 1025               | CHARGE          | invalidEmail       | tony@gmail.com    | 123@111            |
      | HIPERCARD     | 6062825624254001 | 1004            | 1025               | CHARGE          | invalidPhoneNo     | 1234567890        | !@123              |
      | HIPERCARD     | 6062825624254001 | 1004            | 1025               | CHARGE          | invalidState       | PR                | PRESS              |


Feature: SNEI Invalid Field Validations for Visa

  V3.1 charge input should be able to create new customer if a customer has not already exist

##################################################################################################
#    There is no amount validation for EBANX. Transaction gets CHARGED with invalid data         #
#    There is no StateProvince validation for EBANX. Transaction gets CHARGED with invalid data  #
##################################################################################################


###################################################
#    1002/1006 - Ebanx Visa                       #
#    1004/1040 - Astropay Visa                    #
###################################################


    ########################################################################################################################
    #                           Invalid Fields Transaction and Validations                                                 #
    ########################################################################################################################


  Scenario Outline: A charge for Credit Card is being made and the response for error is validated for invalid fields

    When I do a call to post a charge with missing/invalid fields for <paymentMethod>, <CreditCardNo>, <paymentConfigId>, <instrumentConfigId>, <acceptedCommand>, <orignalFieldValue> and <replacedValue>
    And I will validate the <responseValidation> for the above charge

    Examples:
      | paymentMethod | CreditCardNo     | paymentConfigId | instrumentConfigId | acceptedCommand | responseValidation | orignalFieldValue | replacedValue      |
      | VISA          | 4012888888881881 | 1002            | 1006               | CHARGE          | invalidCardNumber  | 4012888888881881  | 12345              |
      | VISA          | 4012888888881881 | 1002            | 1006               | CHARGE          | invalidExpMonth    | 112018            | 132018             |
      | VISA          | 4012888888881881 | 1002            | 1006               | CHARGE          | invalidCVV         | 321               | 12345              |
      | VISA          | 4012888888881881 | 1002            | 1006               | CHARGE          | invalidAmount      | 300               | 11111111111112222  |
      | VISA          | 4012888888881881 | 1002            | 1006               | CHARGE          | invalidCountry     | BR<               | IN<                |
      | VISA          | 4012888888881881 | 1002            | 1006               | CHARGE          | invalidCurrency    | BRL               | INL                |
      | VISA          | 4012888888881881 | 1002            | 1006               | CHARGE          | invalidCPF         | 031.138.219-32    | 123                |
      | VISA          | 4012888888881881 | 1002            | 1006               | CHARGE          | invalidEmail       | tony@gmail.com    | 123@111            |
      | VISA          | 4012888888881881 | 1002            | 1006               | CHARGE          | invalidPhoneNo     | 1234567890        | !@123              |
      | VISA          | 4012888888881881 | 1002            | 1006               | CHARGE          | invalidState       | PR                | PRESS              |

    Examples:
      | paymentMethod | CreditCardNo     | paymentConfigId | instrumentConfigId | acceptedCommand | responseValidation | orignalFieldValue | replacedValue      |
      | VISA          | 4012888888881881 | 1004            | 1040               | CHARGE          | invalidCardNumber  | 4012888888881881  | 12345              |
      | VISA          | 4012888888881881 | 1004            | 1040               | CHARGE          | invalidExpMonth    | 112018            | 132018             |
      | VISA          | 4012888888881881 | 1004            | 1040               | CHARGE          | invalidCVV         | 321               | 12345              |
      | VISA          | 4012888888881881 | 1004            | 1040               | CHARGE          | invalidAmount      | 300               | 11111111111112222  |
      | VISA          | 4012888888881881 | 1004            | 1040               | CHARGE          | invalidCountry     | BR<               | IN<                |
      | VISA          | 4012888888881881 | 1004            | 1040               | CHARGE          | invalidCurrency    | BRL               | INL                |
      | VISA          | 4012888888881881 | 1004            | 1040               | CHARGE          | invalidCPF         | 031.138.219-32    | 123                |
      | VISA          | 4012888888881881 | 1004            | 1040               | CHARGE          | invalidEmail       | tony@gmail.com    | 123@111            |
      | VISA          | 4012888888881881 | 1004            | 1040               | CHARGE          | invalidPhoneNo     | 1234567890        | !@123              |
      | VISA          | 4012888888881881 | 1004            | 1040               | CHARGE          | invalidState       | PR                | PRESS              |


Feature: SNEI Invalid Field Validations for American Express

  V3.1 charge input should be able to create new customer if a customer has not already exist

##################################################################################################
#    There is no amount validation for EBANX. Transaction gets CHARGED with invalid data         #
#    There is no StateProvince validation for EBANX. Transaction gets CHARGED with invalid data  #
##################################################################################################


###################################################
#    1002/1008 - Ebanx American Express           #
#    1004/1042 - Astropay American Express        #
###################################################


    ########################################################################################################################
    #                           Invalid Fields Transaction and Validations                                                 #
    ########################################################################################################################


  Scenario Outline: A charge for Credit Card is being made and the response for error is validated for invalid fields

    When I do a call to post a charge with missing/invalid fields for <paymentMethod>, <CreditCardNo>, <paymentConfigId>, <instrumentConfigId>, <acceptedCommand>, <orignalFieldValue> and <replacedValue>
    And I will validate the <responseValidation> for the above charge

    Examples:
      | paymentMethod    | CreditCardNo    | paymentConfigId | instrumentConfigId | acceptedCommand | responseValidation | orignalFieldValue | replacedValue      |
      | AMERICAN_EXPRESS | 378282246310005 | 1002            | 1008               | CHARGE          | invalidCardNumber  | 378282246310005   | 12345              |
      | AMERICAN_EXPRESS | 378282246310005 | 1002            | 1008               | CHARGE          | invalidExpMonth    | 112018            | 132018             |
      | AMERICAN_EXPRESS | 378282246310005 | 1002            | 1008               | CHARGE          | invalidCVV         | 1234              | 12345              |
      | AMERICAN_EXPRESS | 378282246310005 | 1002            | 1008               | CHARGE          | invalidAmount      | 300               | 11111111111112222  |
      | AMERICAN_EXPRESS | 378282246310005 | 1002            | 1008               | CHARGE          | invalidCountry     | BR<               | IN<                |
      | AMERICAN_EXPRESS | 378282246310005 | 1002            | 1008               | CHARGE          | invalidCurrency    | BRL               | INL                |
      | AMERICAN_EXPRESS | 378282246310005 | 1002            | 1008               | CHARGE          | invalidCPF         | 031.138.219-32    | 123                |
      | AMERICAN_EXPRESS | 378282246310005 | 1002            | 1008               | CHARGE          | invalidEmail       | tony@gmail.com    | 123@111            |
      | AMERICAN_EXPRESS | 378282246310005 | 1002            | 1008               | CHARGE          | invalidPhoneNo     | 1234567890        | !@123              |
      | AMERICAN_EXPRESS | 378282246310005 | 1002            | 1008               | CHARGE          | invalidState       | PR                | PRESS              |

    Examples:
      | paymentMethod    | CreditCardNo    | paymentConfigId | instrumentConfigId | acceptedCommand | responseValidation | orignalFieldValue | replacedValue      |
      | AMERICAN_EXPRESS | 378282246310005 | 1004            | 1042               | CHARGE          | invalidCardNumber  | 378282246310005   | 12345              |
      | AMERICAN_EXPRESS | 378282246310005 | 1004            | 1042               | CHARGE          | invalidExpMonth    | 112018            | 132018             |
      | AMERICAN_EXPRESS | 378282246310005 | 1004            | 1042               | CHARGE          | invalidCVV         | 1234              | 12345              |
      | AMERICAN_EXPRESS | 378282246310005 | 1004            | 1042               | CHARGE          | invalidAmount      | 300               | 11111111111112222  |
      | AMERICAN_EXPRESS | 378282246310005 | 1004            | 1042               | CHARGE          | invalidCountry     | BR<               | IN<                |
      | AMERICAN_EXPRESS | 378282246310005 | 1004            | 1042               | CHARGE          | invalidCurrency    | BRL               | INL                |
      | AMERICAN_EXPRESS | 378282246310005 | 1004            | 1042               | CHARGE          | invalidCPF         | 031.138.219-32    | 123                |
      | AMERICAN_EXPRESS | 378282246310005 | 1004            | 1042               | CHARGE          | invalidEmail       | tony@gmail.com    | 123@111            |
      | AMERICAN_EXPRESS | 378282246310005 | 1004            | 1042               | CHARGE          | invalidPhoneNo     | 1234567890        | !@123              |
      | AMERICAN_EXPRESS | 378282246310005 | 1004            | 1042               | CHARGE          | invalidState       | PR                | PRESS              |


Feature: SNEI Invalid Field Validations for Elo

  V3.1 charge input should be able to create new customer if a customer has not already exist

##################################################################################################
#    There is no amount validation for EBANX. Transaction gets CHARGED with invalid data         #
#    There is no StateProvince validation for EBANX. Transaction gets CHARGED with invalid data  #
##################################################################################################


###################################################
#    1002/1009 - Ebanx Elo                        #
#    1004/1026 - Astropay Elo                     #
###################################################


    ########################################################################################################################
    #                           Invalid Fields Transaction and Validations                                                 #
    ########################################################################################################################


  Scenario Outline: A charge for Credit Card is being made and the response for error is validated for invalid fields

    When I do a call to post a charge with missing/invalid fields for <paymentMethod>, <CreditCardNo>, <paymentConfigId>, <instrumentConfigId>, <acceptedCommand>, <orignalFieldValue> and <replacedValue>
    And I will validate the <responseValidation> for the above charge

    Examples:
      | paymentMethod | CreditCardNo     | paymentConfigId | instrumentConfigId | acceptedCommand | responseValidation | orignalFieldValue | replacedValue      |
      | ELO           | 6362970000457013 | 1002            | 1009               | CHARGE          | invalidCardNumber  | 6362970000457013  | 12345              |
      | ELO           | 6362970000457013 | 1002            | 1009               | CHARGE          | invalidExpMonth    | 112018            | 132018             |
      | ELO           | 6362970000457013 | 1002            | 1009               | CHARGE          | invalidCVV         | 321               | 12345              |
      | ELO           | 6362970000457013 | 1002            | 1009               | CHARGE          | invalidAmount      | 300               | 11111111111112222  |
      | ELO           | 6362970000457013 | 1002            | 1009               | CHARGE          | invalidCountry     | BR<               | IN<                |
      | ELO           | 6362970000457013 | 1002            | 1009               | CHARGE          | invalidCurrency    | BRL               | INL                |
      | ELO           | 6362970000457013 | 1002            | 1009               | CHARGE          | invalidCPF         | 031.138.219-32    | 123                |
      | ELO           | 6362970000457013 | 1002            | 1009               | CHARGE          | invalidEmail       | tony@gmail.com    | 123@111            |
      | ELO           | 6362970000457013 | 1002            | 1009               | CHARGE          | invalidPhoneNo     | 1234567890        | !@123              |
      | ELO           | 6362970000457013 | 1002            | 1009               | CHARGE          | invalidState       | PR                | PRESS              |

    Examples:
      | paymentMethod | CreditCardNo     | paymentConfigId | instrumentConfigId | acceptedCommand | responseValidation | orignalFieldValue | replacedValue      |
      | ELO           | 6362970000457013 | 1004            | 1026               | CHARGE          | invalidCardNumber  | 6362970000457013  | 12345              |
      | ELO           | 6362970000457013 | 1004            | 1026               | CHARGE          | invalidExpMonth    | 112018            | 132018             |
      | ELO           | 6362970000457013 | 1004            | 1026               | CHARGE          | invalidCVV         | 321               | 12345              |
      | ELO           | 6362970000457013 | 1004            | 1026               | CHARGE          | invalidAmount      | 300               | 11111111111112222  |
      | ELO           | 6362970000457013 | 1004            | 1026               | CHARGE          | invalidCountry     | BR<               | IN<                |
      | ELO           | 6362970000457013 | 1004            | 1026               | CHARGE          | invalidCurrency    | BRL               | INL                |
      | ELO           | 6362970000457013 | 1004            | 1026               | CHARGE          | invalidCPF         | 031.138.219-32    | 123                |
      | ELO           | 6362970000457013 | 1004            | 1026               | CHARGE          | invalidEmail       | tony@gmail.com    | 123@111            |
      | ELO           | 6362970000457013 | 1004            | 1026               | CHARGE          | invalidPhoneNo     | 1234567890        | !@123              |
      | ELO           | 6362970000457013 | 1004            | 1026               | CHARGE          | invalidState       | PR                | PRESS              |


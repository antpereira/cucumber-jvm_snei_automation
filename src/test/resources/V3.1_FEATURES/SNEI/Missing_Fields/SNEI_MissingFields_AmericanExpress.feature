Feature: SNEI Missing Field Validations for American Express

  V3.1 charge input should be able to create new customer if a customer has not already exist

######################################################################################################
#    There is no PhoneNo validation for Astropay. Transaction gets CHARGED with missing data         #
#    There is no StateProvince validation for Astropay. Transaction gets CHARGED with missing data   #
######################################################################################################


###################################################
#    1002/1008 - Ebanx American Express           #
#    1004/1042 - Astropay American Express        #
###################################################


    ########################################################################################################################
    #                               Missing Field Transaction with Validations                                             #
    ########################################################################################################################


  Scenario Outline: A charge for Credit Card is being made and the response for error is validated for missing fields

    When I do a call to post a charge with missing/invalid fields for <paymentMethod>, <CreditCardNo>, <paymentConfigId>, <instrumentConfigId>, <acceptedCommand>, <orignalFieldValue> and <replacedValue>
    And I will validate the <responseValidation> for the above charge

    Examples:
      | paymentMethod    | CreditCardNo    | paymentConfigId | instrumentConfigId | acceptedCommand | responseValidation | orignalFieldValue | replacedValue |
      | AMERICAN_EXPRESS | 378282246310005 | 1002            | 1008               | CHARGE          | missingCardNumber  | 378282246310005   | null          |
      | AMERICAN_EXPRESS | 378282246310005 | 1002            | 1008               | CHARGE          | missingExpMonth    | 11                | null          |
      | AMERICAN_EXPRESS | 378282246310005 | 1002            | 1008               | CHARGE          | missingAmount      | 300               | null          |
      | AMERICAN_EXPRESS | 378282246310005 | 1002            | 1008               | CHARGE          | missingCountry     | BR<               | null<         |
      | AMERICAN_EXPRESS | 378282246310005 | 1002            | 1008               | CHARGE          | missingCurrency    | BRL               | null          |
      | AMERICAN_EXPRESS | 378282246310005 | 1002            | 1008               | CHARGE          | missingCPF         | 031.138.219-32    | null          |
      | AMERICAN_EXPRESS | 378282246310005 | 1002            | 1008               | CHARGE          | missingEmail       | tony@gmail.com    | null          |
      | AMERICAN_EXPRESS | 378282246310005 | 1002            | 1008               | CHARGE          | missingPhoneNo     | 1234567890        | null          |
      | AMERICAN_EXPRESS | 378282246310005 | 1002            | 1008               | CHARGE          | missingState       | PR                | null          |

    Examples:
      | paymentMethod    | CreditCardNo    | paymentConfigId | instrumentConfigId | acceptedCommand | responseValidation | orignalFieldValue | replacedValue |
      | AMERICAN_EXPRESS | 378282246310005 | 1004            | 1042               | CHARGE          | missingCardNumber  | 378282246310005   | null          |
      | AMERICAN_EXPRESS | 378282246310005 | 1004            | 1042               | CHARGE          | missingExpMonth    | 11                | null          |
      | AMERICAN_EXPRESS | 378282246310005 | 1004            | 1042               | CHARGE          | missingAmount      | 300               | null          |
      | AMERICAN_EXPRESS | 378282246310005 | 1004            | 1042               | CHARGE          | missingCountry     | BR<               | null<         |
      | AMERICAN_EXPRESS | 378282246310005 | 1004            | 1042               | CHARGE          | missingCurrency    | BRL               | null          |
      | AMERICAN_EXPRESS | 378282246310005 | 1004            | 1042               | CHARGE          | missingCPF         | 031.138.219-32    | null          |
      | AMERICAN_EXPRESS | 378282246310005 | 1004            | 1042               | CHARGE          | missingEmail       | tony@gmail.com    | null          |
      | AMERICAN_EXPRESS | 378282246310005 | 1004            | 1042               | CHARGE          | missingPhoneNo     | 1234567890        | null          |
      | AMERICAN_EXPRESS | 378282246310005 | 1004            | 1042               | CHARGE          | missingState       | PR                | null          |


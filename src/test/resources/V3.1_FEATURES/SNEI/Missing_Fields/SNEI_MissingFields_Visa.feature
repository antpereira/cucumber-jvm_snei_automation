Feature: SNEI Missing Field Validations for Visa

  V3.1 charge input should be able to create new customer if a customer has not already exist

######################################################################################################
#    There is no PhoneNo validation for Astropay. Transaction gets CHARGED with missing data         #
#    There is no StateProvince validation for Astropay. Transaction gets CHARGED with missing data   #
######################################################################################################


###################################################
#    1006/1022 - Ebanx Visa                       #
#    1004/1040 - Astropay Visa                    #
###################################################


    ########################################################################################################################
    #                               Missing Field Transaction with Validations                                             #
    ########################################################################################################################


  Scenario Outline: A charge for Credit Card is being made and the response for error is validated for missing fields

    When I do a call to post a charge with missing/invalid fields for <paymentMethod>, <CreditCardNo>, <paymentConfigId>, <instrumentConfigId>, <acceptedCommand>, <orignalFieldValue> and <replacedValue>
    And I will validate the <responseValidation> for the above charge

    Examples:
      | paymentMethod | CreditCardNo     | paymentConfigId | instrumentConfigId | acceptedCommand | responseValidation | orignalFieldValue | replacedValue |
      | VISA          | 6062825624254001 | 1006            | 1022               | CHARGE          | missingCardNumber  | 6062825624254001  | null          |
      | VISA          | 6062825624254001 | 1006            | 1022               | CHARGE          | missingExpMonth    | 11                | null          |
      | VISA          | 6062825624254001 | 1006            | 1022               | CHARGE          | missingAmount      | 300               | null          |
      | VISA          | 6062825624254001 | 1006            | 1022               | CHARGE          | missingCountry     | BR<               | null<         |
      | VISA          | 6062825624254001 | 1006            | 1022               | CHARGE          | missingCurrency    | BRL               | null          |
      | VISA          | 6062825624254001 | 1006            | 1022               | CHARGE          | missingCPF         | 031.138.219-32    | null          |
      | VISA          | 6062825624254001 | 1006            | 1022               | CHARGE          | missingEmail       | tony@gmail.com    | null          |
      | VISA          | 6062825624254001 | 1006            | 1022               | CHARGE          | missingPhoneNo     | 1234567890        | null          |
      | VISA          | 6062825624254001 | 1006            | 1022               | CHARGE          | missingState       | PR                | null          |

    Examples:
      | paymentMethod | CreditCardNo     | paymentConfigId | instrumentConfigId | acceptedCommand | responseValidation | orignalFieldValue | replacedValue |
      | VISA          | 6062825624254001 | 1004            | 1040               | CHARGE          | missingCardNumber  | 6062825624254001  | null          |
      | VISA          | 6062825624254001 | 1004            | 1040               | CHARGE          | missingExpMonth    | 11                | null          |
      | VISA          | 6062825624254001 | 1004            | 1040               | CHARGE          | missingAmount      | 300               | null          |
      | VISA          | 6062825624254001 | 1004            | 1040               | CHARGE          | missingCountry     | BR<               | null<         |
      | VISA          | 6062825624254001 | 1004            | 1040               | CHARGE          | missingCurrency    | BRL               | null          |
      | VISA          | 6062825624254001 | 1004            | 1040               | CHARGE          | missingCPF         | 031.138.219-32    | null          |
      | VISA          | 6062825624254001 | 1004            | 1040               | CHARGE          | missingEmail       | tony@gmail.com    | null          |
      | VISA          | 6062825624254001 | 1004            | 1040               | CHARGE          | missingPhoneNo     | 1234567890        | null          |
      | VISA          | 6062825624254001 | 1004            | 1040               | CHARGE          | missingState       | PR                | null          |


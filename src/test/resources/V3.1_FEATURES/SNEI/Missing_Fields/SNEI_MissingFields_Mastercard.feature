Feature: SNEI Missing Field Validations for Mastercard

  V3.1 charge input should be able to create new customer if a customer has not already exist

######################################################################################################
#    There is no PhoneNo validation for Astropay. Transaction gets CHARGED with missing data         #
#    There is no StateProvince validation for Astropay. Transaction gets CHARGED with missing data   #
######################################################################################################


###################################################
#    1002/1007 - Ebanx Mastercard                 #
#    1004/1041 - Astropay Mastercard              #
###################################################


    ########################################################################################################################
    #                               Missing Field Transaction with Validations                                             #
    ########################################################################################################################


  Scenario Outline: A charge for Credit Card is being made and the response for error is validated for missing fields

    When I do a call to post a charge with missing/invalid fields for <paymentMethod>, <CreditCardNo>, <paymentConfigId>, <instrumentConfigId>, <acceptedCommand>, <orignalFieldValue> and <replacedValue>
    And I will validate the <responseValidation> for the above charge

    Examples:
      | paymentMethod | CreditCardNo     | paymentConfigId | instrumentConfigId | acceptedCommand | responseValidation | orignalFieldValue | replacedValue |
      | MASTERCARD    | 5105105105105100 | 1002            | 1007               | CHARGE          | missingCardNumber  | 5105105105105100  | null          |
      | MASTERCARD    | 5105105105105100 | 1002            | 1007               | CHARGE          | missingExpMonth    | 11                | null          |
      | MASTERCARD    | 5105105105105100 | 1002            | 1007               | CHARGE          | missingAmount      | 300               | null          |
      | MASTERCARD    | 5105105105105100 | 1002            | 1007               | CHARGE          | missingCountry     | BR<               | null<         |
      | MASTERCARD    | 5105105105105100 | 1002            | 1007               | CHARGE          | missingCurrency    | BRL               | null          |
      | MASTERCARD    | 5105105105105100 | 1002            | 1007               | CHARGE          | missingCPF         | 031.138.219-32    | null          |
      | MASTERCARD    | 5105105105105100 | 1002            | 1007               | CHARGE          | missingEmail       | tony@gmail.com    | null          |
      | MASTERCARD    | 5105105105105100 | 1002            | 1007               | CHARGE          | missingPhoneNo     | 1234567890        | null          |
      | MASTERCARD    | 5105105105105100 | 1002            | 1007               | CHARGE          | missingState       | PR                | null          |

    Examples:
      | paymentMethod | CreditCardNo     | paymentConfigId | instrumentConfigId | acceptedCommand | responseValidation | orignalFieldValue | replacedValue |
      | MASTERCARD    | 5105105105105100 | 1004            | 1041               | CHARGE          | missingCardNumber  | 5105105105105100  | null          |
      | MASTERCARD    | 5105105105105100 | 1004            | 1041               | CHARGE          | missingExpMonth    | 11                | null          |
      | MASTERCARD    | 5105105105105100 | 1004            | 1041               | CHARGE          | missingAmount      | 300               | null          |
      | MASTERCARD    | 5105105105105100 | 1004            | 1041               | CHARGE          | missingCountry     | BR<               | null<         |
      | MASTERCARD    | 5105105105105100 | 1004            | 1041               | CHARGE          | missingCurrency    | BRL               | null          |
      | MASTERCARD    | 5105105105105100 | 1004            | 1041               | CHARGE          | missingCPF         | 031.138.219-32    | null          |
      | MASTERCARD    | 5105105105105100 | 1004            | 1041               | CHARGE          | missingEmail       | tony@gmail.com    | null          |
      | MASTERCARD    | 5105105105105100 | 1004            | 1041               | CHARGE          | missingPhoneNo     | 1234567890        | null          |
      | MASTERCARD    | 5105105105105100 | 1004            | 1041               | CHARGE          | missingState       | PR                | null          |


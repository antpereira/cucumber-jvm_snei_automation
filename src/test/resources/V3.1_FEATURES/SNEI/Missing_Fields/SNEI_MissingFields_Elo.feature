Feature: SNEI Missing Field Validations for Elo

  V3.1 charge input should be able to create new customer if a customer has not already exist

######################################################################################################
#    There is no PhoneNo validation for Astropay. Transaction gets CHARGED with missing data         #
#    There is no StateProvince validation for Astropay. Transaction gets CHARGED with missing data   #
######################################################################################################


###################################################
#    1002/1009 - Ebanx Elo                        #
#    1004/1026 - Astropay Elo                     #
###################################################


    ########################################################################################################################
    #                               Missing Field Transaction with Validations                                             #
    ########################################################################################################################


  Scenario Outline: A charge for Credit Card is being made and the response for error is validated for missing fields

    When I do a call to post a charge with missing/invalid fields for <paymentMethod>, <CreditCardNo>, <paymentConfigId>, <instrumentConfigId>, <acceptedCommand>, <orignalFieldValue> and <replacedValue>
    And I will validate the <responseValidation> for the above charge

    Examples:
      | paymentMethod | CreditCardNo     | paymentConfigId | instrumentConfigId | acceptedCommand | responseValidation | orignalFieldValue | replacedValue |
      | ELO           | 6362970000457013 | 1002            | 1009               | CHARGE          | missingCardNumber  | 6362970000457013  | null          |
      | ELO           | 6362970000457013 | 1002            | 1009               | CHARGE          | missingExpMonth    | 11                | null          |
      | ELO           | 6362970000457013 | 1002            | 1009               | CHARGE          | missingAmount      | 300               | null          |
      | ELO           | 6362970000457013 | 1002            | 1009               | CHARGE          | missingCountry     | BR<               | null<         |
      | ELO           | 6362970000457013 | 1002            | 1009               | CHARGE          | missingCurrency    | BRL               | null          |
      | ELO           | 6362970000457013 | 1002            | 1009               | CHARGE          | missingCPF         | 031.138.219-32    | null          |
      | ELO           | 6362970000457013 | 1002            | 1009               | CHARGE          | missingEmail       | tony@gmail.com    | null          |
      | ELO           | 6362970000457013 | 1002            | 1009               | CHARGE          | missingPhoneNo     | 1234567890        | null          |
      | ELO           | 6362970000457013 | 1002            | 1009               | CHARGE          | missingState       | PR                | null          |

    Examples:
      | paymentMethod | CreditCardNo     | paymentConfigId | instrumentConfigId | acceptedCommand | responseValidation | orignalFieldValue | replacedValue |
      | ELO           | 6362970000457013 | 1004            | 1026               | CHARGE          | missingCardNumber  | 6362970000457013  | null          |
      | ELO           | 6362970000457013 | 1004            | 1026               | CHARGE          | missingExpMonth    | 11                | null          |
      | ELO           | 6362970000457013 | 1004            | 1026               | CHARGE          | missingAmount      | 300               | null          |
      | ELO           | 6362970000457013 | 1004            | 1026               | CHARGE          | missingCountry     | BR<               | null<         |
      | ELO           | 6362970000457013 | 1004            | 1026               | CHARGE          | missingCurrency    | BRL               | null          |
      | ELO           | 6362970000457013 | 1004            | 1026               | CHARGE          | missingCPF         | 031.138.219-32    | null          |
      | ELO           | 6362970000457013 | 1004            | 1026               | CHARGE          | missingEmail       | tony@gmail.com    | null          |
      | ELO           | 6362970000457013 | 1004            | 1026               | CHARGE          | missingPhoneNo     | 1234567890        | null          |
      | ELO           | 6362970000457013 | 1004            | 1026               | CHARGE          | missingState       | PR                | null          |


package implementations;

import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import org.json.simple.JSONObject;
import step_definitions.Hooks;
import utils.Utilities;

import java.awt.*;
import java.io.*;


import static com.jayway.restassured.RestAssured.given;
import static org.testng.reporters.jq.BasePanel.S;
import static utils.Utilities.requestBody;
import static utils.Utilities.requestPath;


public class PostChargeMethod {

    protected String timeStamp = Utilities.getTimestamp();
    protected String signature;
    protected JSONObject jsonRequestBody;
    protected String hooksLink;
    public Response response;
    static String xmlString;

    Server_Configurations configurations = new Server_Configurations();

    protected String requestPath = configurations.requestPath;

    RequestBodyCreator requestbody = new RequestBodyCreator();

    private void setSignatureSNEITest(String CALLER_NAME, String MERCHANT_ACCOUNT, String SECRET_KEY, String timeStamp) throws Exception{

        try{
            signature = Utilities.generateHmacSignatureHexSNEI(CALLER_NAME, MERCHANT_ACCOUNT, timeStamp, SECRET_KEY);

        } catch (Exception e){
            System.out.println("Something went wrong while trying to generate the signature");
            e.printStackTrace();
        }
    }

    public Response signatureGeneration() throws Exception{

        String CALLER_NAME = configurations.CALLER_NAME;
        String MERCHANT_ACCOUNT = configurations.MERCHANT_ACCOUNT;
        String secret = configurations.secret;
        setSignatureSNEITest(CALLER_NAME,MERCHANT_ACCOUNT,secret,timeStamp);

        //REST assured framework to extract response
        response =
                given().    //get ready all the headers and request body to be sent
                        contentType(ContentType.XML).  //contentType is needed for POST and PUT methods as part of MIME
                        accept(ContentType.XML).       //accept is needed for POST and PUT methods as part of MIME
                        headers("X-Live-Gamer-HMAC-Signature", signature ,
                        "X-Live-Gamer-PartnerId", CALLER_NAME,
                        "X-Live-Gamer-AppFamilyId", MERCHANT_ACCOUNT,
                        "X-Live-Gamer-HMAC-Timestamp", timeStamp,
                        "Content-Type","application/xml").
                        body(requestbody.requestBody).
                        when().
                        post(Hooks.apiUrl + requestPath).
                        then().
                        extract().response();

        return response;
    }

    public String requestBodyCreator(String paymentMethod, String CreditCardNo, String paymentConfigId, String instrumentConfigId, String CVV, String acceptedCommand) throws IOException {
        String req1 = requestbody.requestBody.replace("paymentMethod",paymentMethod);
        String req2 = req1.replace("CredicardNo",CreditCardNo);
        String req3 = req2.replace("paymentConfigId",paymentConfigId);
        String req4 = req3.replace("instConfigId",instrumentConfigId);
        String req5 =req4.replace("Cvv",CVV);
        String req6 =req5.replace("acceptedCommand",acceptedCommand);
        return  req6;
    }

    public String requestBodyCreatorNoCVV(String paymentMethod, String CreditCardNo, String paymentConfigId, String instrumentConfigId, String acceptedCommand) throws IOException {
        String req1 = requestbody.requestBodyNoCVV.replace("paymentMethod",paymentMethod);
        String req2 = req1.replace("CredicardNo",CreditCardNo);
        String req3 = req2.replace("paymentConfigId",paymentConfigId);
        String req4 = req3.replace("instConfigId",instrumentConfigId);
        String req5 =req4.replace("acceptedCommand",acceptedCommand);
        return  req5;
    }

    public String requestBodyCreatorMissing(String requestBody, String orignalFieldValue, String replacedValue) throws IOException {
        String missingReuestBody = null;

        switch (replacedValue)
        {
            case "null":
                missingReuestBody = requestBody.replace(orignalFieldValue,"");
                break;
            case ">null":
                missingReuestBody = requestBody.replace(orignalFieldValue,">");
                break;
            case "null<":
                missingReuestBody = requestBody.replace(orignalFieldValue,"<");
                break;
            default:
                missingReuestBody = requestBody.replace(orignalFieldValue, replacedValue);
                break;
        }
        return  missingReuestBody;
    }



    public Response postSNEICharge(String[] fieldArray, String sign, String paymentMethod, String CreditCardNo, String paymentConfigId, String instrumentConfigId, String CVV, String acceptedCommand) throws Exception{

        String CALLER_NAME = configurations.CALLER_NAME;
        String MERCHANT_ACCOUNT = configurations.MERCHANT_ACCOUNT;
        String secret = configurations.secret;

        String requestBody = requestBodyCreator(paymentMethod,CreditCardNo,paymentConfigId,instrumentConfigId, CVV, acceptedCommand);
        System.out.println("\nREQUEST Body For CHARGE/FUNDS_AUTH is as follows: \n \n"+requestBody + "\n");
        //REST assured framework to extract response
        response =
                given().    //get ready all the headers and request body to be sent
                        contentType(ContentType.XML).  //contentType is needed for POST and PUT methods as part of MIME
                        accept(ContentType.XML).       //accept is needed for POST and PUT methods as part of MIME
                        headers("X-Live-Gamer-HMAC-Signature", sign ,
                        "X-Live-Gamer-PartnerId", CALLER_NAME,
                        "X-Live-Gamer-AppFamilyId", MERCHANT_ACCOUNT,
                        "X-Live-Gamer-HMAC-Timestamp", timeStamp,
                        "Content-Type","application/xml").
                        body(requestBody).
                        when().
                        post(Hooks.apiUrl + requestPath).
                        then().
                        extract().response();

        return response;
    }

    public Response postSNEIChargeNoCVV(String[] fieldArray, String sign, String paymentMethod, String CreditCardNo, String paymentConfigId, String instrumentConfigId, String acceptedCommand) throws Exception{

        String CALLER_NAME = configurations.CALLER_NAME;
        String MERCHANT_ACCOUNT = configurations.MERCHANT_ACCOUNT;
        String secret = configurations.secret;

        String requestBody = requestBodyCreatorNoCVV(paymentMethod,CreditCardNo,paymentConfigId,instrumentConfigId, acceptedCommand);
        System.out.println("\nREQUEST Body For CHARGE/FUNDS_AUTH is as follows: \n \n"+requestBody + "\n");
        //REST assured framework to extract response
        response =
                given().    //get ready all the headers and request body to be sent
                        contentType(ContentType.XML).  //contentType is needed for POST and PUT methods as part of MIME
                        accept(ContentType.XML).       //accept is needed for POST and PUT methods as part of MIME
                        headers("X-Live-Gamer-HMAC-Signature", sign ,
                        "X-Live-Gamer-PartnerId", CALLER_NAME,
                        "X-Live-Gamer-AppFamilyId", MERCHANT_ACCOUNT,
                        "X-Live-Gamer-HMAC-Timestamp", timeStamp,
                        "Content-Type","application/xml").
                        body(requestBody).
                        when().
                        post(Hooks.apiUrl + requestPath).
                        then().
                        extract().response();

        return response;
    }

    public Response postUpdateCommandCharge(String chargeId, String updateCommand) throws Exception{

        requestPath = configurations.updateRequestPath + chargeId + "/command";

        String CALLER_NAME = configurations.CALLER_NAME;
        String MERCHANT_ACCOUNT = configurations.MERCHANT_ACCOUNT;
        String secret = configurations.secret;

        String newUpdateCommandRequestBody = requestbody.updateCommandRequestBody.replace("updateCommand",updateCommand);

        setSignatureSNEITest(CALLER_NAME,MERCHANT_ACCOUNT,secret,timeStamp);

        //REST assured framework to extract response
        response =
                given().    //get ready all the headers and request body to be sent
                        contentType(ContentType.XML).  //contentType is needed for POST and PUT methods as part of MIME
                        accept(ContentType.XML).       //accept is needed for POST and PUT methods as part of MIME
                        headers("X-Live-Gamer-HMAC-Signature", signature ,
                        "X-Live-Gamer-PartnerId", CALLER_NAME,
                        "X-Live-Gamer-AppFamilyId", MERCHANT_ACCOUNT,
                        "X-Live-Gamer-HMAC-Timestamp", timeStamp,
                        "Content-Type","application/xml").
                        body(newUpdateCommandRequestBody).
                        when().
                        post(requestPath).
                        then().
                        extract().response();
        return response;
    }


    public Response postSNEIChargeMissingOrInvalidFields(String[] fieldArray, String sign, String paymentMethod, String CreditCardNo, String paymentConfigId, String instrumentConfigId, String CVV, String acceptedCommand, String orignalFieldValue,String replacedValue) throws Exception{

        String CALLER_NAME = configurations.CALLER_NAME;
        String MERCHANT_ACCOUNT = configurations.MERCHANT_ACCOUNT;
        String secret = configurations.secret;

        String requestBody = requestBodyCreator(paymentMethod,CreditCardNo,paymentConfigId,instrumentConfigId, CVV, acceptedCommand);
        String missingRequestBody = requestBodyCreatorMissing(requestBody, orignalFieldValue, replacedValue);

        System.out.println("Request Body is: "+ requestBody);
        //REST assured framework to extract response
        response =
                given().    //get ready all the headers and request body to be sent
                        contentType(ContentType.XML).  //contentType is needed for POST and PUT methods as part of MIME
                        accept(ContentType.XML).       //accept is needed for POST and PUT methods as part of MIME
                        headers("X-Live-Gamer-HMAC-Signature", sign ,
                        "X-Live-Gamer-PartnerId", CALLER_NAME,
                        "X-Live-Gamer-AppFamilyId", MERCHANT_ACCOUNT,
                        "X-Live-Gamer-HMAC-Timestamp", timeStamp,
                        "Content-Type","application/xml").
                        body(missingRequestBody).
                        when().
                        post(Hooks.apiUrl + requestPath).
                        then().
                        extract().response();

        return response;
    }


}

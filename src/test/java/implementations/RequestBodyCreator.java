package implementations;

public class RequestBodyCreator {


  public String  requestBody="<?xml version=\"1.0\"?>\n" +
            "<purchaseOrder ipAddress=\"192.168.103.34\">\n" +
            "    <buyerUser id=\"1006\"></buyerUser>\n" +
            "    <orderCommand>acceptedCommand</orderCommand>\n" +
            "    <presentationInfo description=\"30 MyTokens\"/>\n" +
            "    <lineItems>\n" +
            "        <lineItem>\n" +
            "            <simpleOffer>\n" +
            "                <amount>300</amount>\n" +
            "                <currency>BRL</currency>\n" +
            "            </simpleOffer>\n" +
            "        </lineItem>\n" +
            "    </lineItems>\n" +
            "    <payment configId=\"paymentConfigId\" instrumentConfigId=\"instConfigId\">\n" +
            "        <creditCard>\n" +
            "            <creditCardNumber>CredicardNo</creditCardNumber>\n" +
            "            <creditCardType>paymentMethod</creditCardType>\n" +
            "            <expDate>112018</expDate>\n" +
            "            <securityCode>Cvv</securityCode>\n" +
            "            <firstName>Carmen</firstName>\n" +
            "            <surname>Wardle</surname>\n" +
            "            <billToStreetAddress>AV MIRACATU</billToStreetAddress>\n" +
            "            <billToStreetAddress2>2993</billToStreetAddress2>\n" +
            "            <billToZipCode>81500000</billToZipCode>\n" +
            "            <city>CURITIBA</city>\n" +
            "            <stateProvince>RJ</stateProvince>\n" +
            "            <countryCode>BR</countryCode>\n" +
            "            <phoneNumber>1234567890</phoneNumber>\n" +
            "            <productDescription>Wallet</productDescription>\n" +
            "            <birthDate>03/09/1982</birthDate>\n" +
            "            <document>031.138.219-32</document>\n" +
            "            <email>tony@gmail.com</email>\n" +
            "        </creditCard>\n" +
            "    </payment>\n" +
            "</purchaseOrder>";

  public String requestBodyNoCVV="<?xml version=\"1.0\"?>\n" +
          "<purchaseOrder ipAddress=\"192.168.103.34\">\n" +
          "    <buyerUser id=\"1006\"></buyerUser>\n" +
          "    <orderCommand>acceptedCommand</orderCommand>\n" +
          "    <presentationInfo description=\"30 MyTokens\"/>\n" +
          "    <lineItems>\n" +
          "        <lineItem>\n" +
          "            <simpleOffer>\n" +
          "                <amount>300</amount>\n" +
          "                <currency>BRL</currency>\n" +
          "            </simpleOffer>\n" +
          "        </lineItem>\n" +
          "    </lineItems>\n" +
          "    <payment configId=\"paymentConfigId\" instrumentConfigId=\"instConfigId\">\n" +
          "        <creditCard>\n" +
          "            <creditCardNumber>CredicardNo</creditCardNumber>\n" +
          "            <creditCardType>paymentMethod</creditCardType>\n" +
          "            <expDate>112018</expDate>\n" +
          "            <firstName>Carmen</firstName>\n" +
          "            <surname>Wardle</surname>\n" +
          "            <billToStreetAddress>AV MIRACATU</billToStreetAddress>\n" +
          "            <billToStreetAddress2>2993</billToStreetAddress2>\n" +
          "            <billToZipCode>81500000</billToZipCode>\n" +
          "            <city>CURITIBA</city>\n" +
          "            <stateProvince>RJ</stateProvince>\n" +
          "            <countryCode>BR</countryCode>\n" +
          "            <phoneNumber>1234567890</phoneNumber>\n" +
          "            <productDescription>Wallet</productDescription>\n" +
          "            <birthDate>03/09/1982</birthDate>\n" +
          "            <document>031.138.219-32</document>\n" +
          "            <email>tony@gmail.com</email>\n" +
          "        </creditCard>\n" +
          "    </payment>\n" +
          "</purchaseOrder>";


    public String updateCommandRequestBody = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
            "<purchaseOrderCommand accessKey=\"YwK6678M\" partnerId=\"$apiactor\" appFamilyId=\"1391647949131\" ipAddress=\"255.255.255.255\">\n" +
            "   <commandType>updateCommand</commandType>\n" +
            "</purchaseOrderCommand>";


}

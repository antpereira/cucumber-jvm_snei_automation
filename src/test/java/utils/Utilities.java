package utils;

import org.apache.commons.io.IOUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;


/**
 * Created by apereira on 1/11/2016
 */


//PARAMETER DEFINITIONS
//
//        X-Live-Gamer-CallerName - the name of the caller as defined in the Pay+ admin tool
//        X-Live-Gamer-MerchantAccount - the name of the merchant account
//        X-Live-Gamer-HMAC-Timestamp - current Unix time in seconds. This MUST be in UTC
//        request_path - relative path of API, include query params if present
//        request_content - The HTTP method body if present
//
// For HTTP GET/DELETE: X-Live-Gamer-CallerName + X-Live-Gamer-MerchantAccount + X-Live-Gamer-HMAC-Timestamp + request_path
// For HTTP POST/PUT: X-Live-Gamer-CallerName + X-Live-Gamer-MerchantAccount + X-Live-Gamer-HMAC-Timestamp + request_path + request_content


public class Utilities {
    private static final String HMAC_SHA256_ALGORITHM = "HmacSHA256";
    public static String requestPath;
    public static String requestBody;

    public static String timestamp;
    static String jsonString0;
    static String jsonString1;
    public static String jsonString;
    static String modifiedString;

    public static String getTimestamp(){
        timestamp = String.valueOf(System.currentTimeMillis()/1000);
        return timestamp;
    }

    public static String generateHmacSignatureHexSNEI(String callerName, String merchantAccount, String timestamp, String secretKey
                                                  ) throws Exception{
        Mac mac = Mac.getInstance(HMAC_SHA256_ALGORITHM);
        SecretKeySpec keySpec = new SecretKeySpec(secretKey.getBytes(), HMAC_SHA256_ALGORITHM);

        mac.init(keySpec);

        StringBuilder message = new StringBuilder().append(callerName).append(merchantAccount).append(timestamp);

        byte[] signatureBytes = mac.doFinal(message.toString().getBytes());
        String signature = DatatypeConverter.printHexBinary(signatureBytes);

        return signature;
    }

    public static String generateHmacSignatureHex(String callerName, String merchantAccount, String timestamp, String secretKey,
                                                  String requestPath, String requestBody) throws Exception{
        Mac mac = Mac.getInstance(HMAC_SHA256_ALGORITHM);
        SecretKeySpec keySpec = new SecretKeySpec(secretKey.getBytes(), HMAC_SHA256_ALGORITHM);

        mac.init(keySpec);

        StringBuilder message = new StringBuilder().append(callerName).append(merchantAccount).append(timestamp);

        if (requestPath != null && requestPath.trim().length() > 0) {
            message.append(requestPath);
        }
        if (requestBody != null && requestBody.trim().length() > 0) {
            message.append(requestBody);
        }

        byte[] signatureBytes = mac.doFinal(message.toString().getBytes());
        String signature = DatatypeConverter.printHexBinary(signatureBytes);

        return signature;
    }



}




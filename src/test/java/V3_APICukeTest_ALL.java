import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(monochrome = false, format={"pretty", "json:target/cucumber.json", "html:reports/API_V3.1_Charge.json"},
        features = {"src/test/resources/V3.1_FEATURES"}, tags = {"~@Ignore"})

public class V3_APICukeTest_ALL {

}

package step_definitions;

import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Response;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import implementations.PostChargeMethod;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.runner.notification.RunListener;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Created by kenh on 8/16/2016.
 */
public class Submit_Charge {

    public Response responseFromCharge;
    private String[] changeArray;
    public static String chargeId;
    String threeDigitCVV;
    String fourDigitCVV;
    String CVVcode;

    private final String SCENARIO_BEGINS = "\n---------------Scenario Begins---------------\n";
    private final String SCENARIO_ENDS = "\n---------------Scenario Ends---------------\n";


    public String CVVgenerator(String paymentMethod) throws Exception {
        threeDigitCVV = RandomStringUtils.randomNumeric(3);
        fourDigitCVV = RandomStringUtils.randomNumeric(4);
        if (paymentMethod.equals("AMERICAN_EXPRESS")) {
            CVVcode = "1234";
        } else {
            CVVcode = "321";
        }
        return CVVcode;
    }


    @When("^I do a call to set the signature and use the signature to post a charge with (.+), (.+), (.+), (.+) and (.+)$")
    public void postSNEISignature(String paymentMethod, String CreditCardNo, String paymentConfigId, String instrumentConfigId, String acceptedCommand) throws Throwable {
        System.out.println(SCENARIO_BEGINS);
        String res = "null";
        changeArray = new String[]{};
        PostChargeMethod postChargeMethod = new PostChargeMethod();
        String CVV = CVVgenerator(paymentMethod);
        responseFromCharge = postChargeMethod.postSNEICharge(changeArray, res, paymentMethod, CreditCardNo, paymentConfigId, instrumentConfigId, CVV, acceptedCommand);
        res = responseFromCharge.asString().substring(66, 130);
        responseFromCharge = postChargeMethod.postSNEICharge(changeArray, res, paymentMethod, CreditCardNo, paymentConfigId, instrumentConfigId, CVV, acceptedCommand);
    }

    @When("^I do a call to set the signature and use the signature to post a charge wtihout passing a CVV with (.+), (.+), (.+), (.+) and (.+)$")
    public void postSNEISignatureNoCVV(String paymentMethod, String CreditCardNo, String paymentConfigId, String instrumentConfigId, String acceptedCommand) throws Throwable {
        System.out.println(SCENARIO_BEGINS);
        String res = "null";
        changeArray = new String[]{};
        PostChargeMethod postChargeMethod = new PostChargeMethod();
        responseFromCharge = postChargeMethod.postSNEIChargeNoCVV(changeArray, res, paymentMethod, CreditCardNo, paymentConfigId, instrumentConfigId, acceptedCommand);
        res = responseFromCharge.asString().substring(66, 130);
        responseFromCharge = postChargeMethod.postSNEIChargeNoCVV(changeArray, res, paymentMethod, CreditCardNo, paymentConfigId, instrumentConfigId, acceptedCommand);
    }

    @Then("^I get a CHARGED transaction for either EBANX/ASTROPAY Validations for CHARGE$")
    public void validateSNEIresponseCHARGE() throws Throwable {
        String res = responseFromCharge.asString();
        System.out.println("\nResponse Body for the CHARGED Transaction is as follows:\n \n"+res);
        assertTrue(res.contains("FULFILLED"));
        assertTrue(res.contains("CHARGED"));
        assertTrue(res.contains("VISA") || res.contains("MASTERCARD") || res.contains("AMERICAN_EXPRESS") || res.contains("HIPERCARD") || res.contains("ELO"));
        assertTrue(res.contains("EBANX") || res.contains("AstroPay"));
        assertTrue(res.contains("<responseText>payStatus=CO, txnCode=OK, authcode=null, desc=Accepted</responseText>") || res.contains("desc=Sandbox - Test credit card, transaction captured, acquirer=EBANX, preApv=true(false)</responseText>"));
        System.out.println(SCENARIO_ENDS);
    }

    @And("^I get a FUNDS_AUTHORIZED transaction for either EBANX/ASTROPAY Validations for FUNDS_AUTH$")
    public void validateSNEIresponseFUNDS_AUTH() throws Throwable {
        String res = responseFromCharge.asString();
        System.out.println("\nResponse Body for the FUNDS_AUTHORIZED Transaction is as follows:\n \n"+res);
        assertTrue(res.contains("FUNDS_AUTHORIZATION"));
        assertTrue(res.contains("FUNDS_AUTHORIZED"));
        assertTrue(res.contains("VISA") || res.contains("MASTERCARD") || res.contains("AMERICAN_EXPRESS") || res.contains("HIPERCARD") || res.contains("ELO"));
        assertTrue(res.contains("EBANX") || res.contains("AstroPay"));
        assertTrue(res.contains("<responseText>Success</responseText>") || res.contains("<responseText>payStatus=PE, txnCode=OK, authcode=null, desc=Accepted</responseText>"));
        System.out.println(SCENARIO_ENDS);
    }

    @And("^I do a call to update the CHARGED transaction above with the (.+)$")
    public void validateSNEIupdatecommand(String updateCommand) throws Throwable {
        PostChargeMethod postChargeMethod = new PostChargeMethod();
        String res = responseFromCharge.asString();
        assertTrue(res.contains("CHARGED"));
        chargeId = res.substring(805, 811);
        System.out.println("\nChargeId to be updated is: "+chargeId +"\n");
        responseFromCharge = postChargeMethod.postUpdateCommandCharge(chargeId, updateCommand);
    }

    @And("^I do a call to update the FUNDS_AUTH transaction above with the (.+)$")
    public void validateSNEIupdatecommandFunds_auth(String updateCommand) throws Throwable {
        PostChargeMethod postChargeMethod = new PostChargeMethod();
        String res = responseFromCharge.asString();
        assertTrue(res.contains("FUNDS_AUTHORIZED") || res.contains("CHARGED"));

        switch (updateCommand) {
            case "REFUND":
                chargeId = responseFromCharge.asString().substring(804, 810);
                System.out.println("\nChargeId to be updated is: "+chargeId +"\n");
                break;
            case "CHARGEBACK":
                chargeId = responseFromCharge.asString().substring(804, 810);
                System.out.println("\nChargeId to be updated is: "+chargeId +"\n");
                break;
            case "REVERSE_CHARGEBACK":
                chargeId = responseFromCharge.asString().substring(807, 813);
                System.out.println("\nChargeId to be updated is: "+chargeId +"\n");
                break;
            default:
                chargeId = responseFromCharge.asString().substring(779, 785);
                System.out.println("\nChargeId to be updated is: "+chargeId +"\n");
                break;
        }
        responseFromCharge = postChargeMethod.postUpdateCommandCharge(chargeId, updateCommand);
    }

    @And("^I do a call to update the CHARGED_BACK transaction above with the (.+)$")
    public void validateSNEIchargeback_rev(String updateCommand) throws Throwable {
        PostChargeMethod postChargeMethod = new PostChargeMethod();
        String res = responseFromCharge.asString();
        assertTrue(res.contains("CHARGED"));
        chargeId = responseFromCharge.asString().substring(807, 813);
        System.out.println("\nChargeId to be updated is: "+chargeId +"\n");
        responseFromCharge = postChargeMethod.postUpdateCommandCharge(chargeId, updateCommand);
    }

    @When("^I do a call to post a charge with missing/invalid fields for (.+), (.+), (.+), (.+), (.+), (.+) and (.+)$")
    public void postSNEISignatureMissingFields(String paymentMethod, String CreditCardNo, String paymentConfigId, String instrumentConfigId, String acceptedCommand, String orignalFieldValue, String replacedValue) throws Throwable {
        String res = "null";
        changeArray = new String[]{};
        PostChargeMethod postChargeMethod = new PostChargeMethod();
        String CVV = CVVgenerator(paymentMethod);
        responseFromCharge = postChargeMethod.postSNEIChargeMissingOrInvalidFields(changeArray, res, paymentMethod, CreditCardNo, paymentConfigId, instrumentConfigId, CVV, acceptedCommand, orignalFieldValue, replacedValue);
        res = responseFromCharge.asString().substring(66, 130);
        responseFromCharge = postChargeMethod.postSNEIChargeMissingOrInvalidFields(changeArray, res, paymentMethod, CreditCardNo, paymentConfigId, instrumentConfigId, CVV, acceptedCommand, orignalFieldValue, replacedValue);
    }

    @And("^I get a REFUNDED transaction for either EBANX/ASTROPAY Validations for REFUND$")
    public void validateSNEIresponseRefund() throws Throwable {
        String res = responseFromCharge.asString();
        System.out.println("\nResponse Body for the REFUNDED Transaction is as follows:\n \n"+res);
        assertTrue(res.contains("FULFILLED"));
        assertTrue(res.contains("REFUNDED"));
        assertTrue(res.contains("REFUND"));
        assertTrue(res.contains("APPROVED"));
        assertTrue(res.contains("VISA") || res.contains("MASTERCARD") || res.contains("AMERICAN_EXPRESS") || res.contains("HIPERCARD") || res.contains("ELO"));
        assertTrue(res.contains("EBANX") || res.contains("AstroPay"));
        assertTrue(res.contains("status=OK, result=1, desc=refunded") || res.contains("desc=Sandbox - Test credit card, transaction captured, acquirer=EBANX, preApv=true(false)</responseText>"));
        System.out.println(SCENARIO_ENDS);
    }

    @And("^I get a CHARGED_BACK transaction for either EBANX/ASTROPAY Validations for CHARGEBACK")
    public void validateSNEIresponseChargeback() throws Throwable {
        String res = responseFromCharge.asString();
        System.out.println("\nResponse Body for the CHARGED_BACK Transaction is as follows:\n \n"+res);
        assertTrue(res.contains("FULFILLED"));
        assertTrue(res.contains("CHARGED_BACK"));
        assertTrue(res.contains("CHARGEBACK"));
        assertTrue(res.contains("RECORDED"));
        assertTrue(res.contains("VISA") || res.contains("MASTERCARD") || res.contains("AMERICAN_EXPRESS") || res.contains("HIPERCARD") || res.contains("ELO"));
        assertTrue(res.contains("EBANX") || res.contains("AstroPay"));
        System.out.println(SCENARIO_ENDS);
    }

    @And("^I get a CHARGEBACK_REVERSED transaction for either EBANX/ASTROPAY Validations for REVERSE_CHARGEBACK")
    public void validateSNEIresponseChargeback_reversed() throws Throwable {
        String res = responseFromCharge.asString();
        System.out.println("\nResponse Body for the CHARGEBACK_REVERSED Transaction is as follows:\n \n"+res);
        assertTrue(res.contains("FULFILLED"));
        assertTrue(res.contains("CHARGEBACK_REVERSED"));
        assertTrue(res.contains("RECORDED"));
        assertTrue(res.contains("VISA") || res.contains("MASTERCARD") || res.contains("AMERICAN_EXPRESS") || res.contains("HIPERCARD") || res.contains("ELO"));
        assertTrue(res.contains("EBANX") || res.contains("AstroPay"));
        System.out.println(SCENARIO_ENDS);
    }

    @And("^I get a SUBMITTED transaction for either EBANX/ASTROPAY Validations for CANCEL_FUNDS_AUTHORIZATION")
    public void validateSNEIresponseCancel_Funds_auth() throws Throwable {
        String res = responseFromCharge.asString();
        System.out.println("\nResponse Body for the CANCEL_FUNDS_AUTHORIZATION Transaction is as follows:\n \n"+res);
        assertTrue(res.contains("SUBMITTED"));
        assertTrue(res.contains("CANCEL_FUNDS_AUTHORIZATION"));
        assertTrue(res.contains("APPROVED"));
        assertTrue(res.contains("VISA") || res.contains("MASTERCARD") || res.contains("AMERICAN_EXPRESS") || res.contains("HIPERCARD") || res.contains("ELO"));
        assertTrue(res.contains("EBANX") || res.contains("AstroPay"));
        assertTrue(res.contains("<responseText>status=OK, result=1, desc=cancelled</responseText>") || res.contains("desc=Sandbox - Test credit card, transaction cancelled, acquirer=EBANX, preApv=false(false)</responseText>"));
        System.out.println(SCENARIO_ENDS);
    }

    @And("^I get a CHARGED transaction for either EBANX/ASTROPAY Validations for CAPTURE_FUNDS")
    public void validateSNEIresponseCapture_Funds() throws Throwable {
        String res = responseFromCharge.asString();
        System.out.println("\nResponse Body for the CAPTURE_FUND Transaction is as follows:\n \n"+res);
        assertTrue(res.contains("CHARGED"));
        assertTrue(res.contains("FULFILLED"));
        assertTrue(res.contains("SALE"));
        assertTrue(res.contains("APPROVED"));
        assertTrue(res.contains("VISA") || res.contains("MASTERCARD") || res.contains("AMERICAN_EXPRESS") || res.contains("HIPERCARD") || res.contains("ELO"));
        assertTrue(res.contains("EBANX") || res.contains("AstroPay"));
        assertTrue(res.contains("<responseText>status=OK, result=9, desc=approved</responseText>") || res.contains("desc=Sandbox - Test credit card, transaction captured, acquirer=EBANX, preApv=true(false)</responseText>"));
        System.out.println(SCENARIO_ENDS);
    }


    public void validateCommonMissingResponse() {
        String res = responseFromCharge.asString();
        System.out.println("\nResponse Body for the Missing/Invalid Transaction is as follows:\n \n"+res);
        assertTrue(res.contains("SUBMITTED"));
        assertTrue(res.contains("ERROR"));
        assertTrue(res.contains("VISA") || res.contains("MASTERCARD") || res.contains("AMERICAN_EXPRESS") || res.contains("HIPERCARD") || res.contains("ELO"));
        assertTrue(res.contains("EBANX") || res.contains("AstroPay"));
    }


    @And("^I will validate the (.+) for the above charge$")
    public void validateSNEIresponseMissing(String responseValidation) throws Throwable {
        String res = responseFromCharge.asString();
        System.out.println(SCENARIO_ENDS);

        switch (responseValidation) {
            case "invalidCardNumber":
                validateCommonMissingResponse();
                assertTrue(res.contains("<responseText>Error Code: BP-DR-75 Message: Card number is invalid</responseText>") || res.contains("<responseText>Error Code: BP-DR-75 Message: Card number is invalid Origin Message: Invalid params cc_number</responseText>"));
                break;
            case "invalidExpMonth":
                assertTrue(res.contains("Purchase Order Submission Failed. API Message: credit card exp date should be MMYY or MMYYYY. API CODE: 990002"));
                break;
            case "invalidCVV":
                validateCommonMissingResponse();
                assertTrue(res.contains("<responseText>Error Code: BP-DR-55 Message: Parameter payment.creditcard.card_cvv can have 4 characters maximum Origin Message: Invalid params cc_cvv</responseText>") || res.contains("<responseText>Error Code: BP-DR-55 Message: Parameter payment.card.card_cvv is invalid</responseText>"));
                break;
            case "invalidAmount":
                assertTrue(res.contains("<responseText>Error Code: BP-CAP-10 Message: Invalid amount Origin Message: Invalid params x_amount</responseText>") || res.contains("desc=Sandbox - Test credit card, transaction captured, acquirer=EBANX, preApv=true(false)</responseText>"));
                break;
            case "invalidCountry":
                validateCommonMissingResponse();
                assertTrue(res.contains("<responseText>Error Code: BP-DR-77 Message: Country 'in' is not enabled</responseText>") || res.contains("<responseText>Error Code: DA-0 Message: Unknown error Origin Message: Currency not allowed for this country</responseText>"));
                break;
            case "invalidCurrency":
                assertTrue(res.contains("<message>Invalid currency INL</message>"));
                break;
            case "invalidCPF":
                validateCommonMissingResponse();
                assertTrue(res.contains("<responseText>Error Code: BP-DR-23 Message: Field payment.document must be a valid CPF</responseText>") || res.contains("<responseText>Error Code: BP-DR-23 Message: Field payment.document must be a valid CPF Origin Message: Invalid params x_cpf</responseText>"));
                break;
            case "invalidEmail":
                validateCommonMissingResponse();
                assertTrue(res.contains("<responseText>Error Code: BP-DR-17 Message: Field payment.email must be a valid email</responseText>") || res.contains("<responseText>Error Code: BP-DR-17 Message: Field payment.email must be a valid email Origin Message: Invalid params x_email</responseText>"));
                break;
            case "invalidPhoneNo":
                validateCommonMissingResponse();
                assertTrue(res.contains("<responseText>Error Code: BP-DR-32 Message: Parameter payment.phone_number must be a valid phone number</responseText>") || res.contains("<responseText>Error Code: BP-DR-32 Message: Parameter payment.phone_number must be a valid phone number Origin Message: Invalid params x_phone</responseText>"));
                break;
//            case "invalidState":
//                assertTrue(res.contains("<responseText>status=OK, result=9, desc=approved, ccDescriptor=EmerPay</responseText>") || res.contains("desc=Sandbox - Test credit card, transaction captured, acquirer=EBANX, preApv=true(false)</responseText>"));
//                break;
            case "missingCardNumber":
                validateCommonMissingResponse();
                assertTrue(res.contains("<responseText>Error Code: BP-DR-48 Message: Field payment.card is required for this payment type</responseText>") || res.contains("<responseText>Error Code: BP-DR-48 Message: Field payment.card is required for this payment type Origin Message: Empty params cc_number</responseText>"));
                break;
            case "missingExpMonth":
                assertTrue(res.contains("Purchase Order Submission Failed. API Message: credit card exp date should be MMYY or MMYYYY. API CODE: 990002"));
                break;
            case "missingAmount":
                assertTrue(res.contains("<message>Missing Required Amount Element</message>"));
                break;
            case "missingCountry":
                assertTrue(res.contains("<message>Purchase Order Submission Failed. API Message: No enum constant com.twofish.tempest.data.np.Country.. API CODE: 990002</message>"));
                break;
            case "missingCurrency":
                assertTrue(res.contains("<responseText>Gateway (id: 1002) does not support currency (id: 4)</responseText>") || res.contains("<responseText>Gateway (id: 1004) does not support currency (id: 4)</responseText>"));
                break;
            case "missingCPF":
               // assertTrue(res.contains("DECLINED")); check with jacob on monday throws declined for EBANX but error for Astropay
                assertTrue(res.contains("<responseText>CPF is required in EBANX Direct API request operation!</responseText>") || res.contains("<responseText>Error Code: BP-DR-23 Message: Field payment.document must be a valid CPF Origin Message: Empty params x_cpf</responseText>"));
                break;
            case "missingEmail":
                //validateCommonMissingResponse();
                assertTrue(res.contains("<responseText>Email is required in EBANX Direct API request operation!</responseText>") || res.contains("<responseText>Error Code: BP-DR-15 Message: Field payment.email is required Origin Message: Empty params x_email</responseText>"));
                break;
            case "missingPhoneNo":
                assertTrue(res.contains("<responseText>Phone Number is required in EBANX Direct API request operation!</responseText>") || res.contains("<responseText>payStatus=CO, txnCode=OK, authcode=null, desc=Accepted</responseText>"));
                break;
            case "missingState":
//                assertTrue(res.contains("<responseText>State is required in EBANX Direct API request operation!</responseText>") || res.contains("<responseText>payStatus=CO, txnCode=OK, authcode=null, desc=Accepted</responseText>"));
                break;
            default:
                break;
        }
    }
}
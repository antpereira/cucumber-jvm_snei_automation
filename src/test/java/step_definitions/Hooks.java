package step_definitions;

import cucumber.api.java.Before;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

public class Hooks {
    static RemoteWebDriver driver;

    public static String v3Properties = "src/test/java/properties/v3_api.properties";
    public static String browserStack;
    public static String apiUrl;

    public static String superadminMerchant;
    public static String superadminCaller;
    public static String superadminCred;

    public static String browserConfig;

    @Before
    public void setUp() throws Exception {
        InputStream inGlobal = new FileInputStream(v3Properties);
        Properties propGlobal = new Properties();
        propGlobal.load(inGlobal);

        browserStack=propGlobal.getProperty("browserStack");

        String url=propGlobal.getProperty("apiUrl");
        if(url.contains("tfelements")) {
            apiUrl = "https://" + System.getProperty("api.url", url);
        }
        else{
            apiUrl = "http://" + System.getProperty("api.url", url);
        }

        superadminMerchant=propGlobal.getProperty("superadmin.merchant");
        superadminCaller=propGlobal.getProperty("superadmin.caller");
        superadminCred=propGlobal.getProperty("superadmin.cred");

        inGlobal.close();

        browserConfig="headless";
    }
}
